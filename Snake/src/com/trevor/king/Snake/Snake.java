package com.trevor.king.Snake;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.net.URL;
import java.util.Random;

import javax.swing.JFrame;

import com.trevor.king.Snake.entities.Food;
import com.trevor.king.Snake.entities.SnakeCharacter;
import com.trevor.king.Snake.graphics.Background;
import com.trevor.king.Snake.graphics.Screen;
import com.trevor.king.Snake.graphics.Splash;
import com.trevor.king.Snake.input.Keyboard;

public class Snake extends Canvas implements Runnable {

    private static final long serialVersionUID = 6155721715466693243L;
    public static final String GAME_TITLE = "Snake";
    public static final String GAME_STAGE = "Alpha";
    public static final int GAME_MAJOR_VERSION = 1;
    public static final int GAME_MINOR_VERSION = 0;
    public static final int GAME_REVISION = 0;
    
    public static JFrame frame;

    private boolean running = false;
    private static final boolean debug = false;
    private static final int SPLASH_TIME = 5000; // in ms

    public static final int WIDTH = 800;
    public static final int HEIGHT = WIDTH * 9 / 16;

    private Thread thread;

    private BufferedImage image;
    private int[] pixels;
    private BufferStrategy bs;

    private int fps;
    private int tps;

    private int count;
    private int score;
    private int speed = 4; // 2 = easy, 4 = medium, 6 = hard -- Odd numbers break collision detection...

    public static Screen screen;
    private SnakeCharacter snake;
    private Keyboard key;
    private Food food;
    private Background bg;
    
    public Image icon;

    public enum GameState {
        PAUSED, RUNNING, GAMEOVER, SPLASH
    }

    public GameState gs;

    public Snake() {
        Dimension size = new Dimension(WIDTH, HEIGHT);
        setPreferredSize(size);
        setMaximumSize(size);
        setMinimumSize(size);
        
        image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_ARGB);
        pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
        screen = new Screen(WIDTH, HEIGHT);
        
        URL url = ClassLoader.getSystemResource("SnakeLogo.png");
        Toolkit kit = Toolkit.getDefaultToolkit();
        icon = kit.createImage(url);
    }

    public void init() {
        key = new Keyboard();        
        snake = new SnakeCharacter(WIDTH, HEIGHT, screen, WIDTH / 2, HEIGHT / 2, key, speed);
        bg = new Background("/snakebg.png", WIDTH, HEIGHT, screen);
        
        count = 0;
        score = 0;
        
        gs = GameState.RUNNING;

        addKeyListener(key);
    }

    public synchronized void start() {
        if (running) return;
        running = true;        
        
        if (!debug) splash();
        init();

        thread = new Thread(this);
        thread.start();
    }

    public synchronized void stop() {
        if (!running) return;
        running = false;
        try {
            thread.join(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void quit() {
        if (running) stop();
        System.exit(0);
    }

    private void tick() {
        switch (gs) {
        case PAUSED:
            
            // Wait until key is released before pause handling (prevents holding key)
            while (key.space) {
                key.update();
            }
            
            key.update();
            render();
            if (key.space) {
                gs = GameState.RUNNING;
            }
            
            // Wait until key is released before returning (prevents holding key)
            while (key.space) {
                key.update();
            }
            
            break;

        case GAMEOVER:
            key.update();
            
            if (key.escape) quit();
            else if (key.enter) init();
            
            break;

        case RUNNING:
        default:
            key.update();

            if (food == null) {
                Random r = new Random();
                food = new Food(screen, r.nextInt(WIDTH - Food.SIZE), r.nextInt(HEIGHT - Food.SIZE));
            }

            bg.update();
            snake.update();
            food.update();
            if (snake.ate(food)) {
                count++;
                score = count * speed;
                food = null;
            }
            if (snake.isDead()) gs = GameState.GAMEOVER;

            if (key.space) gs = GameState.PAUSED;
            if (key.ctrl) {
                while (key.ctrl)
                    key.update();
                frame.setState(JFrame.ICONIFIED);
                gs = GameState.PAUSED;
            }
            break;
        }

    }

    private void render() {
        bs = this.getBufferStrategy();
        if (bs == null) {
            createBufferStrategy(3);
            return;
        }

        for (int i = 0; i < pixels.length; i++)
            pixels[i] = screen.pixels[i];

        try {
            Graphics g = bs.getDrawGraphics();
            g.drawImage(image, 0, 0, getWidth(), getHeight(), null);

            renderMessages(g);

            g.dispose();
            bs.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void renderMessages(Graphics g) {        
        if (debug) {
            g.setFont(new Font("", Font.PLAIN, 12));
            g.setColor(Color.BLACK);
            g.fillRect(0, 0, 120, 38);
            g.setColor(Color.GREEN);
            g.drawString("FPS: " + fps, 10, 10);
            g.drawString("TPS: " + tps, 10, 23);
            g.drawString("Score: " + (score), 10, 36);
        }
        
        if (gs == GameState.PAUSED) {
            int fontSize = 36;
            String message = "Press SPACE to resume...";
            int posx = WIDTH / 2 - (message.length() * (fontSize / 4));
            int posy = HEIGHT - fontSize;
            g.setFont(new Font("Arial", Font.BOLD, fontSize));
            g.setColor(Color.RED);
            g.drawString(message, posx, posy);
        }
        
        if (gs == GameState.GAMEOVER) {
            int fontSize = 28;
            String message = "Press ENTER to restart. Press ESC to quit.";
            int posx = WIDTH / 2 - (message.length() * (fontSize / 4));
            int posy = HEIGHT - fontSize;
            g.setFont(new Font("Arial", Font.BOLD, fontSize));
            g.setColor(Color.RED);
            g.drawString(message, posx, posy);
        }
        
        if (gs != GameState.SPLASH) {
            g.setFont(new Font("Arial", Font.BOLD, 24));
            g.setColor(Color.RED);
            g.drawString(String.valueOf(score), 10, 25);
        }
    }

    public void run() {
        requestFocus();
        
        tick();
        gs = GameState.PAUSED;

        long lastTime = System.nanoTime();
        long timer = System.currentTimeMillis();
        final double ns = 1000000000.0 / 60.0;
        double delta = 0;
        int frames = 0;
        int ticks = 0;

        while (running) {
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;

            while (delta >= 1) {
                tick();
                ticks++;
                delta--;
            }

            render();
            frames++;

            if (System.currentTimeMillis() - timer > 1000) {
                timer += 1000;
                tps = ticks;
                fps = frames;
                ticks = 0;
                frames = 0;
            }
        }
    }

    public void splash() {
        GameState oldgs = gs;
        gs = GameState.SPLASH;
        
        Splash splash = new Splash("/splash.png", WIDTH, HEIGHT, screen, SPLASH_TIME);

        double startTime = System.currentTimeMillis();
        double currentTime = startTime;

        while (currentTime - startTime < SPLASH_TIME) {
            splash.update((int) ((currentTime - startTime)));
            render();
            currentTime = System.currentTimeMillis();
        }

        screen.clear();
        
        gs = oldgs;
    }

    public static void main(String[] args) {
        Snake s = new Snake();
        frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        if (debug) frame.setTitle(GAME_TITLE + " | " + GAME_STAGE + " " + GAME_MAJOR_VERSION + "." + GAME_MINOR_VERSION + "." + GAME_REVISION);
        else frame.setTitle(GAME_TITLE);
        frame.setIconImage(s.icon);
        frame.add(s);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        s.start();
    }
}
