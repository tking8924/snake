package com.trevor.king.Snake.entities;

import java.awt.Point;

import com.trevor.king.Snake.graphics.Screen;

public class Food extends RenderedEntity{
    
    private int color = 0xfffefefe;
    public static final int SIZE = 15;

    public Food(Screen screen, int posX, int posY) {
        super(SIZE, SIZE, screen);
        this.posX = posX;
        this.posY = posY;
        
        render();
        draw();
    }
    
    private void render() {
        for (int i = 0; i < pixels.length; i++) 
            pixels[i] = color;
    }
    
    public boolean checkCollision(Point p, int psize) {
        if (p.x > (posX - psize) && (posX + SIZE) > p.x && p.y > (posY- psize) && (posY + SIZE) > p.y) {
            return true;
        }
        else return false;
    }

}
