package com.trevor.king.Snake.entities;

import com.trevor.king.Snake.graphics.Screen;

public class RenderedEntity {
    
    protected int posX = 0;
    protected int posY = 0;
    protected int width;
    protected int height;
    protected Screen screen;
    
    protected int[] pixels;
    
    public RenderedEntity(int width, int height, Screen screen) {
        this.width = width;
        this.height = height;
        this.screen = screen;
        pixels = new int[width * height];
    }

    public void draw() {
        screen.render(posX, posY, width, height, pixels);
    }
    
    public void update() {
        draw();
    }    
}
