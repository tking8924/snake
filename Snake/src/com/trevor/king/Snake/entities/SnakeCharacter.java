package com.trevor.king.Snake.entities;

import java.awt.Point;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;

import com.trevor.king.Snake.graphics.Screen;
import com.trevor.king.Snake.input.Keyboard;

public class SnakeCharacter extends RenderedEntity {

    private int size = 15;
    private static final int GROW_SPEED = 10;
    private int speed;
    private int xDirection = 1;
    private int yDirection = 0;
    private int width;
    private int height;
    private int count = 0;
    private static final int COLOR = 0xff00ff00;
    private static final int HEAD_COLOR = 0xff00a000;
    private boolean eating = false;
    private Keyboard key;

    Deque<Point> body = new ArrayDeque<Point>();

    public SnakeCharacter(int width, int height, Screen screen, int startX, int startY, Keyboard key, int speed) {
        super(width, height, screen);

        this.key = key;
        this.speed = speed;
        this.width = width;
        this.height = height;
        for (int i = 0; i < 5 * size; i++)
            body.add(new Point(startX - (i * speed), startY));

        for (int i = 0; i < pixels.length; i++)
            pixels[i] = -1;
    }

    public void update() {

        if (key.up && !(yDirection == 1)) {
            yDirection = -1;
            xDirection = 0;
        }
        if (key.down && !(yDirection == -1)) {
            yDirection = 1;
            xDirection = 0;
        }
        if (key.right && !(xDirection == -1)) {
            xDirection = 1;
            yDirection = 0;
        }
        if (key.left && !(xDirection == 1)) {
            xDirection = -1;
            yDirection = 0;
        }

        int x = body.peekFirst().x + (xDirection * speed);
        int y = body.peekFirst().y + (yDirection * speed);

        if (x + size > width) x = 0;
        if (x < 0) x = width - size;
        if (y + size > height) y = 0;
        if (y < 0) y = height - size;

        body.offerFirst(new Point(x, y));
        if (!eating) clearSquare(body.pollLast());
        else {
            if (count == GROW_SPEED)  {
                eating = false;
                count = 0;
            }
            else count++;
        }
        
        Iterator<Point> it = body.descendingIterator();
        while(it.hasNext()) {
            Point p = it.next();
            if (it.hasNext()) 
                drawSquare(p, COLOR); 
            else 
                drawSquare(p, HEAD_COLOR); // Different colour for head... probably think of a better way to do this later
        }
            

        draw();
    }
    
    public boolean ate(Food f) {
        boolean ate = f.checkCollision(body.peekFirst(), size);
        if (ate) eating = true;
        return ate;
    }
    
    public boolean isDead() {
        Point start = body.peekFirst();
        
        int x;
        int y;
        
        if (xDirection == 1) { //right
            x = start.x + size;
            y = start.y;
        } else if (xDirection == -1) { //left
            x = start.x;
            y = start.y;
        } else if (yDirection == -1) { //up
            x = start.x;
            y = start.y;
        } else { //down
            x = start.x;
            y = start.y + size;
        }
        
        Iterator<Point> it = body.iterator();
        while (it.hasNext()) {
            Point p = it.next();
            if (yDirection == -1) {
                if (x > (p.x - size) && (p.x + size) > x && y < (p.y + size) && y > (p.y + size - speed)) return true;
            } else if (xDirection == -1) {
                if (x < (p.x + size) && x > (p.x + size - speed) && y > (p.y - size) && (p.y + size) > y) return true;
            } else if (yDirection == 1) {
                if (x > (p.x - size) && (p.x + size) > x && y > (p.y) && y < (p.y + speed)) return true;
            } else if (xDirection == 1) {
                if (x > (p.x) && x < (p.x + speed) && y > (p.y - size) && (p.y + size) > y) return true;
            }
        }
        
        return false;
    }

    private void drawSquare(Point p, int color) {
        int x = p.x;
        int y = p.y;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                int xx = x + i;
                int yy = y + j;
                pixels[xx + yy * width] = color;
            }
        }
    }

    private void clearSquare(Point p) {
        int x = p.x;
        int y = p.y;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                int xx = x + i;
                int yy = y + j;
                pixels[xx + yy * width] = -1;
            }
        }
    }
    
    public Point[] getPoints() {
        Point[] bodyarray = new Point[body.size()];
        bodyarray = body.toArray(bodyarray);
        return bodyarray;
    }
}