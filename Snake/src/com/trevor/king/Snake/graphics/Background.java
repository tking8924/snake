package com.trevor.king.Snake.graphics;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.trevor.king.Snake.entities.RenderedEntity;

public class Background extends RenderedEntity {
    
    private BufferedImage image;

    public Background(String path, int sizeX, int sizeY, Screen screen) {
        super(sizeX, sizeY, screen);
        
        try {
            image = ImageIO.read(Background.class.getResource(path));
            int w = image.getWidth();
            int h = image.getHeight();
            image.getRGB(0, 0, w, h, pixels, 0, w);
        } catch (IOException e) {
            e.printStackTrace();
        }        
    }    
}
