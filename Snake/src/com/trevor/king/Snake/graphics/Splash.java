package com.trevor.king.Snake.graphics;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.trevor.king.Snake.entities.RenderedEntity;

public class Splash extends RenderedEntity{
    
    private BufferedImage image;
    private double splashTime;
    private int[] imagePixels;
    private double fadeTime;
    private double fadeIncrease;

    public Splash(String path, int sizeX, int sizeY, Screen screen, double splashTime) {
        super(sizeX, sizeY, screen);
        this.splashTime = splashTime;
        imagePixels = new int[sizeX * sizeY];
        
        clear();
        
        fadeTime = splashTime / 3;
        fadeIncrease = 16 / fadeTime;
        
        try {
            image = ImageIO.read(Splash.class.getResource(path));
            Image scaledImage = image.getScaledInstance(sizeX, sizeY, Image.SCALE_DEFAULT);
            image = new BufferedImage(sizeX, sizeY, image.getType());
            image.getGraphics().drawImage(scaledImage, 0, 0, null);
            int w = image.getWidth();
            int h = image.getHeight();
            image.getRGB(0, 0, w, h, imagePixels, 0, w);
        } catch (IOException e) {
            e.printStackTrace();
        }        
    }
    
    public void update() {
        update(-1);
    }
    
    private void clear() {
        for (int i = 0; i < pixels.length; i++) pixels[i] = 0xff000000;
    }
    
    public void update(int elap) {
        if (elap == -1) {
            pixels = imagePixels;
        } else {
            if (elap <= splashTime / 3) {
                int fade = (int)(elap * fadeIncrease);  // sets the fade level (0 - 15)
                if (fade > 0xff) fade = 0xff; // if the fade level > 15, set to 15
                fade = fade << 24; // shift the fade (alpha, AA) level left 24 bits to match 0xAARRGGBB
                fade = fade | 0x00ffffff; // bitwise or to ensure that all bits after the fade are 1
                for (int i = 0; i < pixels.length; i++)
                    pixels[i] = imagePixels[i] & fade; // for each pixel, bitwise and with the fade to apply the effect
            } else if (splashTime - elap <= splashTime / 3) {
                clear(); // clear so it will fade to black
                int fade = (int) ((splashTime - elap) * fadeIncrease);
                if (fade > 0xff) fade = 0xff;
                fade = fade << 24;
                fade = fade | 0x00ffffff;
                for (int i = 0; i < pixels.length; i++)
                    pixels[i] = imagePixels[i] & fade;
            } else {
                pixels = imagePixels;
            }
                
        }
        draw();
    }
}
